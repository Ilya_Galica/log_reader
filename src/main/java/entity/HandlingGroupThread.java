package entity;

import dao.FileReaderDAO;
import dao.impl.LogReaderDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.FileReaderService;
import service.exception.ServiceException;
import service.impl.LogReaderServiceImpl;
import service.util.ServiceUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

public class HandlingGroupThread implements Callable<Map<String, Integer>> {

    private static final Logger logger = LogManager.getLogger();
    private static final Properties properties = new Properties();
    private static final FileReaderService readerService = LogReaderServiceImpl.getInstance();
    private String fileName;
    private static Lock locker;
    private int filterParamIndex;

    public HandlingGroupThread(String fileName, int filterParamIndex) {
        this.fileName = fileName;
        this.filterParamIndex = filterParamIndex;
    }

    public static void setLocker(Lock locker) {
        HandlingGroupThread.locker = locker;
    }

    @Override
    public Map<String, Integer> call() throws Exception {
        Map<String, Integer> records = new ConcurrentHashMap<>();
        locker.lock();
        try (FileReaderDAO readerDAO = new LogReaderDAOImpl()) {
            readerDAO.initReader(properties.getProperty("path.to.logs.directory")
                    + "/" + fileName);
            while (readerDAO.getReader().ready()) {
                String line = readerService.readFileLine(readerDAO);
                List<String> tokens = ServiceUtil.splitRecord(line);
                String parameter = tokens.get(filterParamIndex).trim();
                records.computeIfPresent(parameter, (string, integer) -> integer + 1);
                records.putIfAbsent(parameter, 1);
            }
        } catch (ServiceException e) {
            logger.error(e);
        } finally {
            locker.unlock();
        }
        return records;
    }

    static {
        String rootPath = "src/main/resources/appPaths.properties";
        try {
            properties
                    .load(new FileInputStream(rootPath));
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
