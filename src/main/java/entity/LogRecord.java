package entity;

public class LogRecord {
    private final String time;
    private final String username;
    private final String message;

    private LogRecord(final String time, final String username, final String message) {
        this.time = time;
        this.username = username;
        this.message = message;
    }

    public String getTime() {
        return time;
    }


    public String getUsername() {
        return username;
    }


    public String getMessage() {
        return message;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogRecord logRecord = (LogRecord) o;

        if (time != null ? !time.equals(logRecord.time) : logRecord.time != null) return false;
        if (username != null ? !username.equals(logRecord.username) : logRecord.username != null) return false;
        return message != null ? message.equals(logRecord.message) : logRecord.message == null;
    }

    @Override
    public int hashCode() {
        int result = time != null ? time.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogRecord{" +
                "time='" + time + '\'' +
                ", username='" + username + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public static class LogBuilder {
        private String nestedTime;
        private String nestedUsername;
        private String nestedMessage;

        public LogBuilder setNestedTime(String time) {
            this.nestedTime = time;
            return this;
        }

        public LogBuilder setNestedUsername(String username) {
            this.nestedUsername = username;
            return this;
        }

        public LogBuilder setNestedMessage(String message) {
            this.nestedMessage = message;
            return this;
        }

        public LogRecord build() {
            return new LogRecord(nestedTime, nestedUsername, nestedMessage);
        }
    }
}
