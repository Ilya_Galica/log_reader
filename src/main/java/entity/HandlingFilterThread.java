package entity;

import dao.FileReaderDAO;
import dao.impl.LogReaderDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.FileReaderService;
import service.exception.ServiceException;
import service.impl.LogReaderServiceImpl;
import service.util.ServiceUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;

public class HandlingFilterThread implements Callable<List<LogRecord>> {

    private static final Logger logger = LogManager.getLogger();
    private static final Properties properties = new Properties();
    private static final FileReaderService readerService = LogReaderServiceImpl.getInstance();
    private static String parameter;
    private String fileName;
    private static Lock locker;
    private int filterParam;

    public HandlingFilterThread(String fileName, int filterParam) {
        this.fileName = fileName;
        this.filterParam = filterParam;
    }

    public static void setParameter(String parameter) {
        HandlingFilterThread.parameter = parameter;
    }

    public static void setLocker(Lock locker) {
        HandlingFilterThread.locker = locker;
    }

    @Override
    public List<LogRecord> call() throws Exception {
        List<LogRecord> records = new CopyOnWriteArrayList<>();
        locker.lock();
        try (FileReaderDAO readerDAO = new LogReaderDAOImpl()) {
            readerDAO.initReader(properties
                    .getProperty("path.to.logs.directory") + "/" + fileName);
            while (readerDAO.getReader().ready()) {
                String line = readerService.readFileLine(readerDAO);
                List<String> tokens = ServiceUtil.splitRecord(line);
                checkParamValidity(records, tokens);
            }
        } catch (ServiceException e) {
            logger.error(e);
        } finally {
            locker.unlock();
        }
        return records;
    }

    private void checkParamValidity(List<LogRecord> records, List<String> tokens) {
        switch (filterParam) {
            case 1:
                if (tokens.get(filterParam) != null
                        && tokens.get(filterParam).trim().equalsIgnoreCase(parameter)) {
                    addNewLogRecord(records, tokens);
                }
                break;
            case 0:
                if (tokens.get(filterParam) != null
                        && tokens.get(filterParam).trim().contains(parameter)) {
                    addNewLogRecord(records, tokens);
                }
                break;
            case 2:
                if (tokens.get(filterParam) != null
                        && tokens.get(filterParam).trim().contains(parameter)) {
                    addNewLogRecord(records, tokens);
                }
                break;
        }
    }

    private static void addNewLogRecord(List<LogRecord> records, List<String> tokens) {
        records.add(new LogRecord.LogBuilder()
                .setNestedTime(tokens.get(0).trim())
                .setNestedUsername(tokens.get(1).trim())
                .setNestedMessage(tokens.get(2).trim())
                .build());
    }

    static {
        String rootPath = "src/main/resources/appPaths.properties";
        try {
            properties
                    .load(new FileInputStream(rootPath));
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
