package dao.impl;

import dao.FileReaderDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Optional;

public class LogReaderDAOImpl implements FileReaderDAO {

    private static final Logger logger = LogManager.getLogger();
    private BufferedReader reader;


    @Override
    public void initReader(String path) {
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        } catch (FileNotFoundException e) {
            logger.error(e);
        }
    }

    @Override
    public Optional<String> read() {
        try {
            return Optional.ofNullable(reader.readLine());
        } catch (IOException e) {
            logger.error(e);
        }
        return Optional.empty();
    }

    @Override
    public BufferedReader getReader() {
        return reader;
    }

    @Override
    public void close() {
        try {
            reader.close();
        } catch (IOException e) {
            logger.error("Exception due closing reader stream", e);
        }
    }
}
