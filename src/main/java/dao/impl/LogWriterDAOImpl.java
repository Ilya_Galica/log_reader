package dao.impl;

import dao.FileWriterDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class LogWriterDAOImpl implements FileWriterDAO {

    private static final Logger logger = LogManager.getLogger();
    private BufferedWriter writer;


    @Override
    public void write(String data) {
        try {
            writer.write(data);
        } catch (IOException e) {
            logger.error("Exception due write data in file", e);
        }
    }

    @Override
    public void initWriter(String filePath) {
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath)));
        } catch (IOException e) {
            logger.error("Exception due initialising writer stream", e);
        }
    }


    @Override
    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            logger.error("Exception due closing writer stream", e);
        }
    }
}
