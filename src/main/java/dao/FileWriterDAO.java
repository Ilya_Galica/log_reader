package dao;

public interface FileWriterDAO extends AutoCloseable {
    void write(String data);

    void initWriter(String filePath);
}
