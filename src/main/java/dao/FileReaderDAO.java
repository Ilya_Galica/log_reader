package dao;

import java.io.BufferedReader;
import java.util.Optional;

public interface FileReaderDAO extends AutoCloseable {
    void initReader(String path);

    Optional<String> read();

    BufferedReader getReader();
}
