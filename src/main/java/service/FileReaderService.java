package service;

import dao.FileReaderDAO;
import service.exception.ServiceException;

import java.util.List;

public interface FileReaderService {
    List<String> directoryScan(String path);

    String readFileLine(FileReaderDAO fileReaderDAO) throws ServiceException;
}
