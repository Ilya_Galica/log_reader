package service;

import entity.LogRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.impl.LogWriterServiceImpl;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class UserTerminal {
    private static final Logger logger = LogManager.getLogger();
    private static final int TIME_INDEX = 0;
    private static final int USERNAME_INDEX = 1;
    private static final int MESSAGE_INDEX = 2;
    private static final UserTerminal instance = new UserTerminal();
    private static final FileFilterService filterService = FileFilterService.getInstance();
    private static final FileGroupService groupService = FileGroupService.getInstance();
    private static final FileWriterService writerService = LogWriterServiceImpl.getInstance();
    private boolean loopRunning = true;
    private static boolean goBack = false;
    private static boolean matches = false;

    private UserTerminal() {
    }

    public static UserTerminal getInstance() {
        return instance;
    }

    public void doUserChoice(int userChoice) throws IOException {
        while (loopRunning) {
            if (goBack) {
                UserTerminalMenu.giveUserChoiceMenu();
                userChoice = InputUserData.enterNumber();
                goBack = false;
            }
            switch (userChoice) {
                case 1:
                    UserTerminalMenu.giveUserFilterRequest();
                    System.out.println("Do your choice:");
                    handleUserFilterRequest(InputUserData.enterNumber());
                    break;
                case 2:
                    UserTerminalMenu.giveUserGroupRequest();
                    System.out.println("Do your choice:");
                    handleUserGroupRequest(InputUserData.enterNumber());
                    break;
                case 3:
                    System.out.println("Goodbye");
                    loopRunning = false;
                    return;
                default:
                    System.out.println("Check your input");
                    break;
            }
            if (loopRunning && matches) {
                UserTerminalMenu.giveUserChoiceMenu();
            }
            matches = false;
        }
    }

    private void handleUserFilterRequest(int userChoice) {
        switch (userChoice) {
            case 1:
                System.out.println("Enter username:");
                doSingleFilter(USERNAME_INDEX);
                break;
            case 2:
                System.out.println("Enter time:");
                doSingleFilter(TIME_INDEX);
                break;
            case 3:
                System.out.println("Enter part of message:");
                doSingleFilter(MESSAGE_INDEX);
                break;
            case 4:
                doDoubleFilter(USERNAME_INDEX, TIME_INDEX);
                break;
            case 5:
                doDoubleFilter(TIME_INDEX, MESSAGE_INDEX);
                break;
            case 6:
                doDoubleFilter(MESSAGE_INDEX, USERNAME_INDEX);
                break;
            case 7:
                doTripleFilter(USERNAME_INDEX, TIME_INDEX, MESSAGE_INDEX);
                break;
            case 8:
                goBack = true;
                return;
            case 9:
                loopRunning = false;
                System.out.println("Goodbye");
                return;
            default:
                System.out.println("Check your input\n");
                break;
        }
    }

    private void handleUserGroupRequest(int userChoice) {
        switch (userChoice) {
            case 1:
                groupingRecords(USERNAME_INDEX);
                break;
            case 2:
                groupingRecords(TIME_INDEX);
                break;
            case 3:
                groupingRecords(MESSAGE_INDEX);
                break;
            case 4:
                goBack = true;
                return;
            case 5:
                loopRunning = false;
                System.out.println("Goodbye");
                return;
            default:
                System.out.println("Check your input");
                break;
        }
    }

    private static void doSingleFilter(int parameterIndex) {
        try {
            List<LogRecord> records;
            String parameter = InputUserData.enterLine();
            FileFilterService.setFilterParamIndex(parameterIndex);
            records = filterService.filterByParam(parameter);
            doFilterCondition(records);
        } catch (IOException e) {
            logger.error("Exception due entering the line", e);
        }
    }

    private static void doDoubleFilter(int firstParameterIndex, int secondParameterIndex) {
        try {
            List<LogRecord> records;
            System.out.println("Enter first parameter:");
            String firstParameter = InputUserData.enterLine();
            System.out.println("Enter second parameter:");
            String secondParameter = InputUserData.enterLine();
            FileFilterService.setFilterParamIndex(firstParameterIndex);
            if ((records = filterService.filterByParam(firstParameter)).isEmpty()) {
                System.out.println("Didn't find any matches");
            } else {
                doMultiplyFilterCondition(records, secondParameter, secondParameterIndex);
            }
        } catch (IOException e) {
            logger.error("Exception due entering the line", e);
        }
    }

    private static void doTripleFilter(int firstParameterIndex, int secondParameterIndex, int thirdParameterIndex) {
        try {
            List<LogRecord> records;
            System.out.println("Enter first parameter:");
            String firstParameter = InputUserData.enterLine();
            System.out.println("Enter second parameter:");
            String secondParameter = InputUserData.enterLine();
            System.out.println("Enter third parameter:");
            String thirdParameter = InputUserData.enterLine();
            FileFilterService.setFilterParamIndex(firstParameterIndex);
            records = filterService.filterByParam(firstParameter);
            if (records.isEmpty()) {
                matches = false;
                System.out.println("Didn't find any matches");
            } else {
                FileFilterService.setFilterParamIndex(secondParameterIndex);
                records = filterService.multiplyFilter(records, secondParameter);
                if (records.isEmpty()) {
                    matches = false;
                    System.out.println("Didn't find any matches");
                } else {
                    doMultiplyFilterCondition(records, thirdParameter, thirdParameterIndex);
                }
            }
        } catch (IOException e) {
            logger.error("Exception due entering the line", e);
        }
    }

    private static void doMultiplyFilterCondition(List<LogRecord> records, String parameter, int parameterIndex) {
        FileFilterService.setFilterParamIndex(parameterIndex);
        records = filterService.multiplyFilter(records, parameter);
        doFilterCondition(records);
    }

    private static void doFilterCondition(List<LogRecord> records) {
        if (records.isEmpty()) {
            matches = false;
            System.out.println("Didn't find any matches\n");
        } else {
            System.out.println("Data was recorded in file outputFiltered.log\n"
                    + "(src/main/resources/output/outputFiltered.log)");
            writerService.writeFilteredData(records);
            outputOnDesktop("src/main/resources/output/outputFiltered.log");
        }
    }

    private static void groupingRecords(int parameterIndex) {
        Map<String, Integer> records = groupService.groupByParameter();
        FileGroupService.setGroupParamIndex(parameterIndex);
        if (records.isEmpty()) {
            matches = false;
            System.out.println("Didn't find any matches");
        } else {
            writerService.writeGroupedData(records, parameterIndex);
            System.out.println("Data was recorded in file outputGrouped.log\n"
                    + "(src/main/resources/output/outputGrouped.log)");
            outputOnDesktop("src/main/resources/output/outputGrouped.log");
        }
    }

    private static void outputOnDesktop(String filePath) {
        System.out.println("\nDo you wanna open that file? (TYPE \'yes\' for watch" +
                " and \'no\' or something else for go to main menu)");
        try {
            String agreement = InputUserData.enterLine();
            if (agreement.equalsIgnoreCase("yes")) {
                if (!Desktop.isDesktopSupported()) {
                    System.out.println("Sorry, but in your PC desktop is not supported");
                    return;
                }
                Desktop desktop = Desktop.getDesktop();
                File file = new File(filePath);
                if (file.exists()) {
                    desktop.open(file);
                }
                goBack = true;
            } else {
                goBack = true;
            }
        } catch (IOException e) {
            logger.error(e);
        }
    }

}
