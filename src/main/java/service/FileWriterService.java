package service;

import entity.LogRecord;

import java.util.List;
import java.util.Map;

public interface FileWriterService {

    void writeFilteredData(List<LogRecord> records);

    void writeGroupedData(Map<String, Integer> records, int filterParamIndex);

}
