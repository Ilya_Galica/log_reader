package service;

import entity.HandlingFilterThread;
import entity.HandlingGroupThread;
import entity.LogRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.omg.PortableInterceptor.INACTIVE;
import service.impl.LogReaderServiceImpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FileGroupService {

    private static final Logger logger = LogManager.getLogger();
    private static final Properties properties = new Properties();
    private static final FileGroupService instance = new FileGroupService();
    private static final FileReaderService readerService = LogReaderServiceImpl.getInstance();
    private static int groupParamIndex;

    public static FileGroupService getInstance() {
        return instance;
    }

    public Map<String, Integer> groupByParameter() {
        Map<String, Integer> records = new ConcurrentHashMap<>();
        List<String> fileNames = readerService
                .directoryScan(properties.getProperty("path.to.logs.directory"));
        Lock locker = new ReentrantLock();
        HandlingGroupThread.setLocker(locker);
        HandlingGroupThread thread;
        Future<Map<String, Integer>> future;
        ExecutorService executorService = Executors.newFixedThreadPool(fileNames.size());
        for (String fileName : fileNames) {
            thread = new HandlingGroupThread(fileName, groupParamIndex);
            future = executorService.submit(thread);
            try {
                Map<String, Integer> futureRecords = future.get();
                for (Map.Entry<String, Integer> entry : futureRecords.entrySet()) {
                    records.computeIfPresent(entry.getKey(), (line, integer) -> integer + entry.getValue());
                    records.putIfAbsent(entry.getKey(), entry.getValue());
                }
            } catch (InterruptedException | ExecutionException e) {
                logger.error(e);
                executorService.shutdownNow();
            }
        }
        shutdownThread(executorService);
        return records;
    }

    private static void shutdownThread(ExecutorService executorService) {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }
    }

    public static void setGroupParamIndex(int groupParamIndex) {
        FileGroupService.groupParamIndex = groupParamIndex;
    }

    private FileGroupService() {
    }

    static {
        String rootPath = "src/main/resources/appPaths.properties";
        try {
            properties
                    .load(new FileInputStream(rootPath));
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
