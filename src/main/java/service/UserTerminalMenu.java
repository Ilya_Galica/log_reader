package service;

public final class UserTerminalMenu {

    private UserTerminalMenu() {
    }

    public static void giveUserChoiceMenu() {
        System.out.println("Press 1 for filter logs by parameter/parameters");
        System.out.println("Press 2 for group parameters from log");
        System.out.println("Press 3 for exit");
        System.out.println();
    }

    static void giveUserFilterRequest() {
        System.out.println("Press 1 for filter logs by username");
        System.out.println("Press 2 for filter logs by time");
        System.out.println("Press 3 for filter logs by part of message");
        System.out.println("Press 4 for filter logs by username and time");
        System.out.println("Press 5 for filter logs by time and part of message");
        System.out.println("Press 6 for filter logs by username and part of message");
        System.out.println("Press 7 for filter logs by username and part of message and time");
        System.out.println("Press 8 for go back");
        System.out.println("Press 9 for exit");
        System.out.println();
    }

    static void giveUserGroupRequest() {
        System.out.println("Press 1 for group logs by username");
        System.out.println("Press 2 for group logs by time");
        System.out.println("Press 3 for group logs by part of message");
        System.out.println("Press 4 for go back");
        System.out.println("Press 5 for exit");
        System.out.println();
    }
}
