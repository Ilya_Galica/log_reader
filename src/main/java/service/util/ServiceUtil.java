package service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ServiceUtil {
    private ServiceUtil() {
    }

    public static List<String> splitRecord(String record) {
        return new ArrayList<>(Arrays.asList(record.split(",")));
    }

}
