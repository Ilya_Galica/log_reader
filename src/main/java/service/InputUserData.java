package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class InputUserData {

    private static BufferedReader reader;
    private final static String FOR_INPUT_INT = "[\\d]{0,8}";
    private final static String FOR_INPUT_LINE = "([\\w\\s\\-\\_])+";
    private final static Logger logger = LogManager.getLogger();

    public static void installReader() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public static int enterNumber() throws IOException {
        int number;
        String input;
        input = reader.readLine();
        while (isNotValidInput(input, FOR_INPUT_INT)) {
            System.out.println("!!!Please, enter correct number!!!");
            input = reader.readLine();
        }
        number = Integer.parseInt(input);
        return number;
    }

    public static String enterLine() throws IOException {
        String input;
        input = reader.readLine();
        while (isNotValidInput(input, FOR_INPUT_LINE)) {
            System.out.println("!!!Please, enter correct line!!!");
            input = reader.readLine();
        }
        return input;
    }

    private static boolean isNotValidInput(String input, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        return !matcher.matches();
    }

    public static void closeReader() {
        try {
            reader.close();
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
