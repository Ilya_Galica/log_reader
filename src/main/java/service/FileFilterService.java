package service;

import entity.HandlingFilterThread;
import entity.LogRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.impl.LogReaderServiceImpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class FileFilterService {

    private static final Logger logger = LogManager.getLogger();
    private static final Properties properties = new Properties();
    private static final FileFilterService instance = new FileFilterService();
    private static LogReaderServiceImpl readerService = LogReaderServiceImpl.getInstance();
    private static int filterParamIndex;

    public static FileFilterService getInstance() {
        return instance;
    }

    public static void setFilterParamIndex(int filterParamIndex) {
        FileFilterService.filterParamIndex = filterParamIndex;
    }

    public List<LogRecord> filterByParam(String filterParameter) {
        List<LogRecord> records = new CopyOnWriteArrayList<>();
        List<String> fileNames = readerService
                .directoryScan(properties.getProperty("path.to.logs.directory"));
        Lock locker = new ReentrantLock();
        HandlingFilterThread.setParameter(filterParameter);
        HandlingFilterThread.setLocker(locker);
        HandlingFilterThread thread;
        Future<List<LogRecord>> recordsFuture;
        ExecutorService executorService = Executors.newFixedThreadPool(fileNames.size());
        for (String fileName : fileNames) {
            thread = new HandlingFilterThread(fileName, filterParamIndex);
            recordsFuture = executorService.submit(thread);
            try {
                records.addAll(recordsFuture.get());
            } catch (InterruptedException | ExecutionException e) {
                logger.error(e);
                executorService.shutdownNow();
            }
        }
        shutdownThread(executorService);
        return records;
    }

    public List<LogRecord> multiplyFilter(List<LogRecord> records, String filterParameter) {
        switch (filterParamIndex) {
            case 1:
                return records.stream()
                        .filter(logRecord -> logRecord.getUsername().equals(filterParameter))
                        .collect(Collectors.toList());
            case 0:
                return records.stream()
                        .filter(logRecord1 -> logRecord1.getTime().contains(filterParameter))
                        .collect(Collectors.toList());
            case 2:
                return records.stream()
                        .filter(logRecord -> logRecord.getMessage().contains(filterParameter))
                        .collect(Collectors.toList());
        }
        return records;
    }

    private static void shutdownThread(ExecutorService executorService) {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }
    }

    private FileFilterService() {
    }

    static {
        String rootPath = "src/main/resources/appPaths.properties";
        try {
            properties
                    .load(new FileInputStream(rootPath));
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
