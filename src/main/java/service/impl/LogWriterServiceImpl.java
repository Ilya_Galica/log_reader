package service.impl;

import dao.FileWriterDAO;
import dao.impl.LogWriterDAOImpl;
import entity.LogRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.FileWriterService;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class LogWriterServiceImpl implements FileWriterService {

    private static final Logger logger = LogManager.getLogger();
    private static final Properties properties = new Properties();
    private static final LogWriterServiceImpl instance = new LogWriterServiceImpl();

    public static LogWriterServiceImpl getInstance() {
        return instance;
    }

    @Override
    public void writeFilteredData(List<LogRecord> records) {
        try (FileWriterDAO writerDAO = new LogWriterDAOImpl()) {
            writerDAO.initWriter(properties.getProperty("path.to.filtered.file"));
            writerDAO.write("   Time     Username                  Message               \n");
            writerDAO.write("____________________________________________________________\n");
            for (LogRecord record : records) {
                writerDAO.write("____________________________________________________________\n");
                writerDAO.write(record.getTime() + "  " + record.getUsername() + "  " + record.getMessage() + "\n");
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    public void writeGroupedData(Map<String, Integer> records, int filterParamIndex) {
        try (FileWriterDAO writerDAO = new LogWriterDAOImpl()) {
            writerDAO.initWriter(properties.getProperty("path.to.grouped.file"));
            writerDAO.write(getHeader(filterParamIndex));
            for (Map.Entry record : records.entrySet()) {
                writerDAO.write(record.getKey() + "      " + record.getValue() + "\n");
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

    private static String getHeader(int filterParamIndex) {
        switch (filterParamIndex) {
            case 1:
                return "|Username       Count of records |\n";
            case 0:
                return "| Time         Count of records |\n";
            case 2:
                return "|          Message              Count of records |\n";
        }
        return "unknown";
    }

    private LogWriterServiceImpl() {
    }

    static {
        String rootPath = "src/main/resources/appPaths.properties";
        try {
            properties
                    .load(new FileInputStream(rootPath));
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
