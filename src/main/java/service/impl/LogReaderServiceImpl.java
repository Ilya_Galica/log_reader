package service.impl;

import dao.FileReaderDAO;
import service.FileReaderService;
import service.exception.ServiceException;

import java.io.File;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LogReaderServiceImpl implements FileReaderService {

    private static final LogReaderServiceImpl instance = new LogReaderServiceImpl();
    private static final String SUFFIX = ".log";

    public static LogReaderServiceImpl getInstance() {
        return instance;
    }

    @Override
    public List<String> directoryScan(String path) {
        List<String> results = new ArrayList<>();
        File file = new File(path);
        if (file.exists()) {
            Optional<File[]> files
                    = Optional.ofNullable(file.listFiles());
            files.ifPresent(presentFiles -> Arrays.stream(presentFiles)
                    .filter(File::isFile)
                    .map(File::getName)
                    .filter(fileName -> fileName
                            .length() > SUFFIX.length())
                    .filter(fileName -> fileName
                            .substring(fileName.length() - SUFFIX.length())
                            .contains(SUFFIX))
                    .forEach(results::add));
            return results;
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public String readFileLine(FileReaderDAO fileReaderDAO) throws ServiceException {
        Lock locker = new ReentrantLock();
        locker.lock();
        try {
            Optional<String> readLine = fileReaderDAO.read();
            return readLine.orElseThrow(() -> new ServiceException("Empty line"));
        } finally {
            locker.unlock();
        }
    }

    private LogReaderServiceImpl() {
    }
}
