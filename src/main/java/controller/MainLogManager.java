package controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.InputUserData;
import service.UserTerminal;
import service.UserTerminalMenu;

import java.io.IOException;

public class MainLogManager {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        UserTerminal terminal = UserTerminal.getInstance();
        try {
            InputUserData.installReader();
            UserTerminalMenu.giveUserChoiceMenu();
            terminal.doUserChoice(InputUserData.enterNumber());
            InputUserData.closeReader();
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
