package service;

import entity.LogRecord;
import org.testng.annotations.Test;
import service.exception.ServiceException;

import static org.testng.Assert.*;

public class FileFilterServiceTest {

    @Test
    public void testFilterByUsername() {
        FileFilterService fileFilter = FileFilterService.getInstance();
        FileFilterService.setFilterParamIndex(1);
        LogRecord actual
                = fileFilter.filterByParam("firstName").get(0);
        LogRecord expected = new LogRecord.LogBuilder()
                .setNestedTime("2019-09-25")
                .setNestedUsername(("firstName"))
                .setNestedMessage("Error reading the request body")
                .build();
        assertEquals(actual, expected);
    }

    @Test
    public void testFilterByTime() {
        FileFilterService fileFilter = FileFilterService.getInstance();
        FileFilterService.setFilterParamIndex(0);
        LogRecord actual
                = fileFilter.filterByParam("2019-09-25").get(0);
        LogRecord expected = new LogRecord.LogBuilder()
                .setNestedTime("2019-09-25")
                .setNestedUsername(("firstName"))
                .setNestedMessage("Error reading the request body")
                .build();
        assertEquals(actual, expected);
    }

    @Test
    public void testFilterByMessage() {
        FileFilterService fileFilter = FileFilterService.getInstance();
        FileFilterService.setFilterParamIndex(2);
        LogRecord actual
                = fileFilter.filterByParam("Error reading the request").get(0);
        LogRecord expected = new LogRecord.LogBuilder()
                .setNestedTime("2019-09-25")
                .setNestedUsername(("firstName"))
                .setNestedMessage("Error reading the request body")
                .build();
        assertEquals(actual, expected);
    }
}