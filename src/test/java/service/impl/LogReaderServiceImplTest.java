package service.impl;

import dao.FileReaderDAO;
import dao.impl.LogReaderDAOImpl;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import service.FileReaderService;
import service.exception.ServiceException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

import static org.testng.Assert.*;

public class LogReaderServiceImplTest {
    private static final Properties properties = new Properties();

    @BeforeTest
    private void config() throws IOException {
        String rootPath = "target/classes/appPaths.properties";
        properties.load(new FileInputStream(rootPath));

    }

    @Test
    public void testDirectoryScan() {
        FileReaderService fileReaderService = LogReaderServiceImpl.getInstance();
        List<String> files = fileReaderService
                .directoryScan(properties.getProperty("path.to.logs.directory"));
        System.out.println(files);
        assertNotNull(files);
    }

    @Test
    public void testReadFileLine() throws ServiceException {
        FileReaderDAO fileReaderDAO = new LogReaderDAOImpl();
        FileReaderService fileReaderService = LogReaderServiceImpl.getInstance();
        String filePath = fileReaderService
                .directoryScan(properties.getProperty("path.to.logs.directory")).get(0);
        fileReaderDAO.initReader(properties
                .getProperty("path.to.logs.directory") + "/" + filePath);
        String actual
                = fileReaderService
                .readFileLine(fileReaderDAO);
        String expected = "2019-09-25, firstName, Error reading the request body";
        assertEquals(actual, expected);
    }
}