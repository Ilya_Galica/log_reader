package service.impl;

import org.testng.annotations.Test;
import service.FileFilterService;
import service.FileGroupService;

public class LogWriterServiceImplTest {

    @Test
    public void testWriteFilteredData()  {
        LogWriterServiceImpl writerService = LogWriterServiceImpl.getInstance();
        FileFilterService.setFilterParamIndex(1);
        FileFilterService filterService = FileFilterService.getInstance();
        writerService.writeFilteredData(filterService
                .filterByParam("firstName"));
    }

    @Test
    public void testWriteGroupedData()  {
        LogWriterServiceImpl writerService = LogWriterServiceImpl.getInstance();
        FileGroupService.setGroupParamIndex(1);
        FileGroupService filterService = FileGroupService.getInstance();
        writerService.writeGroupedData(filterService
                .groupByParameter(), 1);
    }
}