package service;

import org.testng.annotations.Test;
import service.exception.ServiceException;

import java.util.concurrent.ConcurrentHashMap;

import static org.testng.Assert.*;

public class FileGroupServiceTest {

    @Test
    public void testGroupByUsername(){
        FileGroupService fileGroup = FileGroupService.getInstance();
        FileGroupService.setGroupParamIndex(1);
        Integer actual
                = fileGroup.groupByParameter().get("firstName");
        Integer expected = 14;
        assertEquals(actual, expected);
    }

    @Test
    public void testGroupByTime() {
        FileGroupService fileGroup = FileGroupService.getInstance();
        FileGroupService.setGroupParamIndex(0);
        Integer actual
                = fileGroup.groupByParameter().get("2019-09-26");
        Integer expected = 2;
        assertEquals(actual, expected);
    }

    @Test
    public void testGroupByMessage() {
        FileGroupService fileGroup = FileGroupService.getInstance();
        FileGroupService.setGroupParamIndex(2);
        Integer actual
                = fileGroup.groupByParameter().get("Invalid password");
        Integer expected = 9;
        assertEquals(actual, expected);
    }
}